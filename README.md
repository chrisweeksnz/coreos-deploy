# Overview

This project facilitates CoreOS ISO installs.

# Method

* Assemble host
* Connect host to a network that has internet routing and HTTPS outbound access
* Boot host with a linux live environment (USB/CD). 
  * [Solus Budgie](https://solus-project.com/download/) is a good option.
  * Use gnome-multi-writer to write it to your USB stick.
* Open a terminal a run:

    ```shell
    sudo -i
    eopkg ur
    eopkg install xfsprogs
    gparted &
    # Create partition table on /dev/sdb
    # Create /dev/sdb1  with type 'lvm2 pv'
    # Apply, then quit
    vgcreate vg0 /dev/sdb1
    lvcreate --size 200G --thin vg0/backup
    lvs       # verify the lv is correctly created
    mkfs.xfs /dev/vg0/backup
    
    wget https://raw.github.com/coreos/init/master/bin/coreos-install
    chmod 777 coreos-install
    
    # Ensure ignition uses XFS for root
    # https://coreos.com/ignition/docs/latest/examples.html#xfs
    wget https://gitlab.com/chrisweeksnz/coreos-deploy/raw/master/ignition-cwbounce.json
    ./coreos-install -d /dev/sda -i ignition-cwbounce.json

    

    # Add fstab entry to mount 

    # set up systemd timer and service to rsync /docker to /backup/docker
    ```

# Actions to maintain this project

* update container-linux-config (clc) files
* transpile clc files to ignition json files after changes using ct binary from CoreOS
 
### Change this project to have a build container with coreos ct in it, to automatically create the ignition files.


# Links

* https://github.com/coreos/container-linux-config-transpiler/releases
* https://coreos.com/os/docs/latest/installing-to-disk.html
* https://coreos.com/os/docs/latest/clc-examples.html
* https://coreos.com/ignition/docs/latest/examples.html
